Chrome extension to block ads on Pandora.

Installation instructions:
1. Download a *.crx release.
2. Open chrome://extensions/ in a new tab.
3. Drag the downloaded .crx file onto the chrome://extensions/ tab.

See complete instructions here:
http://support.google.com/chrome_webstore/bin/answer.py?hl=en&answer=2664769&p=crx_warning

