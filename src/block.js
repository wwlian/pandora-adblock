// Copyright 2013 Wilson Lian

var filter = {"urls": ["*://www.pandora.com/radio/util/mediaserverPublicRedirect.jsp*",
  "*://www.pandora.com/util/mediaserverPublicRedirect.jsp*",
  "*://www.pandora.com/radio/util/mediaserverPublicRedirect.jsp*",
  "*://www.pandora.com/util/mediaserverPublicRedirect.jsp*",
  "*://ad.doubleclick.net/pfadx/pand.default/prod.tunervideo*"]};
chrome.webRequest.onBeforeRequest.addListener(function (details) {return {"cancel": true};}, filter, ["blocking"]);

chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
    // Reload if deemed inactive.
    if ('url' in changeInfo && changeInfo.url.indexOf("http://www.pandora.com/inactive") >= 0) {
      chrome.tabs.reload(tabId);
    }
  });
